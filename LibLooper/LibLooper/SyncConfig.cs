﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace LibLooper
{
    [DataContract]
    public class SyncConfig
    {
        public SyncConfig()
        {

        }

        public static void WriteConfig(SyncConfig input, string filePath)
        {
            FileStream fs = new FileStream(filePath, FileMode.Create);
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(SyncConfig));
            ser.WriteObject(fs, input);
            fs.Close();
        }

        public static SyncConfig LoadConfig(string filePath)
        {
            try
            {
                FileStream fs = new FileStream(filePath, FileMode.Open);
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(SyncConfig));
                SyncConfig output = (SyncConfig)ser.ReadObject(fs);
                fs.Close();
                if (output != null)
                {
                    return output;
                }
                else
                {
                    SyncConfig newConfig = new SyncConfig();
                    //newConfig.SoundFiles.Add("notification", "C:\\windows\\Media\\Alarm10.wav");
                    if (!File.Exists(filePath))
                    {
                        SyncConfig.WriteConfig(newConfig, filePath);
                    }
                    return newConfig;
                }
            }
            catch
            {
                SyncConfig newConfig = new SyncConfig();
                //newConfig.SoundFiles.Add("notification", "C:\\windows\\Media\\Alarm10.wav");
                if (!File.Exists(filePath))
                {
                    SyncConfig.WriteConfig(newConfig, filePath);
                }
                return newConfig;
            }
        }

        public bool PlaySound
        {
            get
            {
                return playSound;
            }
            set
            {
                playSound = value;
            }
        }

        public List<LooperConfig> AllLoopers
        {
            get
            {
                return allLoopers;
            }
            set
            {
                allLoopers = value;
            }
        }

        [DataMember]
        private bool playSound = false;

        [DataMember]
        private List<LooperConfig> allLoopers = new List<LooperConfig>();
    }

    [DataContract]
    public class LooperConfig
    {
        public LooperConfig()
        {

        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string SoundFile
        {
            get
            {
                return soundFile;
            }
            set
            {
                soundFile = value;
            }
        }

        public string Query
        {
            get
            {
                return query;
            }
            set
            {
                query = value;
            }
        }

        public string Source
        {
            get
            {
                return source;
            }
            set
            {
                source = value;
            }
        }

        public bool DefaultRunning
        {
            get
            {
                return defaultRunning;
            }
            set
            {
                defaultRunning = value;
            }
        }

        public string ConfigType
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        public PrevQuery PreviousQuery
        {
            get
            {
                return previousQuery;
            }
            set
            {
                previousQuery = value;
            }
        }

        public bool HasPreviousQuery
        {
            get
            {
                if (previousQuery != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool Finish
        {
            get
            {
                return finish;
            }
            set
            {
                finish = value;
            }
        }

        public List<FieldValue> StateFields
        {
            get
            {
                return stateFields;
            }
            set
            {
                stateFields = value;
            }
        }

        public List<Severity> SlaTimes
        {
            get
            {
                return slaTimes;
            }
            set
            {
                slaTimes = value;
            }
        }

        public bool FlashScreenOnNotification
        {
            get
            {
                return flashScreenOnNotification;
            }
            set
            {
                flashScreenOnNotification = value;
            }
        }

        [DataMember]
        private string name = "";

        [DataMember]
        private string soundFile = "";

        [DataMember]
        private string query = "";

        [DataMember]
        private string source = "";

        [DataMember]
        private bool defaultRunning = false;

        [DataMember]
        private string type = "looper";

        [DataMember]
        private PrevQuery previousQuery = new PrevQuery();

        [DataMember]
        private bool finish = false;

        [DataMember]
        private List<FieldValue> stateFields = new List<FieldValue>();

        [DataMember]
        private List<Severity> slaTimes = new List<Severity>();

        [DataMember]
        private bool flashScreenOnNotification = false;
    }

    [DataContract]
    public class Severity
    {
        [DataMember]
        public int severity { get; set; }
        [DataMember]
        public int time { get; set; }
        [DataMember]
        public string field { get; set; }
        [DataMember]
        public string actualState { get; set; }
        [DataMember]
        public int warningTime { get; set; }
        [DataMember]
        public int showTime { get; set; }
    }

    [DataContract]
    public class FieldValue
    {
        [DataMember]
        public string Field { get; set; }
        [DataMember]
        public string Value { get; set; }
    }

    [DataContract]
    public class PrevQuery
    {
        public string CheckQuery
        {
            get
            {
                return checkQuery;
            }
            set
            {
                checkQuery = value;
            }
        }

        public string CheckState
        {
            get
            {
                return checkState;
            }
            set
            {
                checkState = value;
            }
        }

        public string AlternateQuery
        {
            get
            {
                return alternateQuery;
            }
            set
            {
                alternateQuery = value;
            }
        }
        [DataMember]
        private string checkQuery = "";
        [DataMember]
        private string checkState = "";
        [DataMember]
        private string alternateQuery = "";
    }
}