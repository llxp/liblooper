﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LibLooper
{
    /*
     * tracker for sending the state of availability (for escorts) to fairfaxpowerbi
     */
    public class Tracker
    {
        public string Server
        {
            get
            {
                return powerBIServer;
            }
            set
            {
                powerBIServer = value;
            }
        }

        public string Database
        {
            get
            {
                return powerBIDatabase;
            }
            set
            {
                powerBIDatabase = value;
            }
        }

        public string Table
        {
            get
            {
                return powerBITable;
            }
            set
            {
                powerBITable = value;
            }
        }

        public string Username
        {
            get
            {
                return powerBIUsername;
            }
            set
            {
                powerBIUsername = value;
            }
        }

        public string Password
        {
            get
            {
                return powerBIPassword;
            }
            set
            {
                powerBIPassword = value;
            }
        }

        public int ConnectionTimeout
        {
            get
            {
                return powerBIConnectionTimeout;
            }
            set
            {
                powerBIConnectionTimeout = value;
            }
        }

        public bool CancelAllLoopers
        {
            get
            {
                return cancelAllLoopers;
            }
            set
            {
                cancelAllLoopers = value;
            }
        }

        public bool userLoggedIn
        {
            get
            {
                if ((DateTime.Now - lastUserLoginCheck) > TimeSpan.FromSeconds(1.5))
                {
                    lastUserLoginCheck = DateTime.Now;
                    userLoggedInState = getuserLoginState();
                }
                return !userLoggedInState;
            }
        }

        /*
         * The data for the connection to the fairfaxpowerbi-server (copied from the powershell script)
         */
        private string powerBIServer = "iyv422ckyb.database.windows.net,1433";
        private string powerBIDatabase = "Fairfax_PowerBI";
        private string powerBITable = "Escort_Active_Operators";
        private string powerBIUsername = "FairfaxPowerBIUser";
        private string powerBIPassword = "W{zV49SW";
        private int powerBIConnectionTimeout = 60;
        DateTime lastUserLoginCheck = DateTime.Now;
        private bool userLoggedInState = false;
        private bool registerUserThreadRunning = false;
        private Thread registerUserThread;
        private bool cancelAllLoopers = false;

        public Tracker()
        {
            /*
             * Initialises the registerUserThread. It registers the current logged in user at the fairfax powerbi
             */
            registerUserThread = new Thread(registerUserThreadFunction);
            registerUserThread.Start();
        }
        /*
         * The Method will register the current user in FairfaxPowerbi, when he/she is logged in
         */
        private void registerEscorter()
        {
            /*if (!userLoggedIn == false)
            {
                return;
            }*/
            string username = Guid.NewGuid().ToString();//Environment.UserName;
            string domain = Environment.UserDomainName;
            string displayName = System.DirectoryServices.AccountManagement.UserPrincipal.Current.DisplayName;
            string cloudEnvironment = "Blackforest";
            if (displayName.ToUpperInvariant().Contains("LOCKHEED") || displayName.ToUpperInvariant().Contains("TATA"))
            {
                cloudEnvironment = "Fairfax";
            }
            else if (displayName.ToUpperInvariant().Contains("T-SYSTEMS"))
            {
                cloudEnvironment = "Blackforest";
            }
            else if (username.Contains("v-"))
            {
                cloudEnvironment = "Mooncake";
            }
            else
            {
                cloudEnvironment = "Not a vendor";
            }
            string powerBIConnectionString = string.Format("Server={0};Database={1};User ID={2};Password={3};Trusted_Connection=False;Encrypt=True;Connect Timeout={4}", powerBIServer, powerBIDatabase, powerBIUsername, powerBIPassword, powerBIConnectionTimeout);
            string sqlCmd = @"IF EXISTS (select Alias from " + powerBITable + " where Alias = '" + username + @"' )
	                        BEGIN
                                UPDATE " + powerBITable + @"
                                SET
                                    Alias = '" + username + @"',
                                    LastActive = GetDate(),
                                    Environment = '" + cloudEnvironment + @"'
                                WHERE Alias = '" + username + @"'
                            END
                        ELSE
                            BEGIN
                            INSERT INTO " + powerBITable + @"
                                    (Alias, LastActive, Environment)
                                VALUES
                                    ('" + username + "', GetDate(), '" + cloudEnvironment + @"')
                            END";
            var powerBIConnection = new SqlConnection();
            powerBIConnection.ConnectionString = powerBIConnectionString;
            powerBIConnection.Open();

            var updateCmd = new SqlCommand();
            updateCmd.Connection = powerBIConnection;
            updateCmd.CommandText = sqlCmd;
            try
            {
                int updateCmdResult = updateCmd.ExecuteNonQuery();
            }
            catch
            {

            }

            powerBIConnection.Close();
        }

        /*
         * This function is executed in the registerUserThread Thread
         */
        private void registerUserThreadFunction()
        {
            while (true)
            {
                while (registerUserThreadRunning)
                {
                    if (cancelAllLoopers)
                    {
                        return;
                    }
                    registerEscorter();//Registering the escorter for availability
                    System.Threading.Thread.Sleep(60 * 1000);
                }
                while (!registerUserThreadRunning)
                {
                    if (cancelAllLoopers)
                    {
                        return;
                    }
                    System.Threading.Thread.Sleep(100);
                }
            }
        }

        /*
         * This function deterines if a user is either logged in or the loginscreen is activated e.g. when the screen is locked
         */
        private bool getuserLoginState(string logonProcess = "LogonUI")
        {
            Process[] procList = Process.GetProcesses();
            if (procList != null)
            {
                foreach (Process proc in procList)
                {
                    if (proc != null)
                    {
                        if (proc.ProcessName.ToLowerInvariant().Contains(logonProcess.ToLowerInvariant()))
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public void Start()
        {
            registerUserThreadRunning = true;
            cancelAllLoopers = false;
            if (registerUserThread == null)
            {
                registerUserThread = new Thread(registerUserThreadFunction);
                registerUserThread.Start();
            }
        }

        public void Pause()
        {
            registerUserThreadRunning = false;
        }

        public void Stop()
        {
            cancelAllLoopers = true;
            registerUserThread = null;
        }
    }
}
