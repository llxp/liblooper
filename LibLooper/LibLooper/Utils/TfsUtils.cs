﻿using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LibLooper.Utils
{
    internal static class TfsUtils
    {
        private static readonly object m_connectAzureTfsLocker = new object();
        public static WorkItemStore connectAzureTfs(Uri uri)
        {
            Monitor.Enter(m_connectAzureTfsLocker);
            try
            {
                TfsTeamProjectCollection ttpcf = new TfsTeamProjectCollection(uri);
                ttpcf.Authenticate();
                var wis = ttpcf.GetService<Microsoft.TeamFoundation.WorkItemTracking.Client.WorkItemStore>();
                return wis;
            }
            catch (Exception ex)
            {
                Logger.logExceptionToFile("TfsUtils.connectAzureTfs", ex);
                return null;
            }
            finally
            {
                Monitor.Exit(m_connectAzureTfsLocker);
            }
        }
    }
}
