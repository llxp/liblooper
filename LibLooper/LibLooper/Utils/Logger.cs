﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibLooper.Utils
{
    public class Logger
    {
        public static void logExceptionToFile(object sender, Exception ex)
        {
            using (StreamWriter file = new StreamWriter("error.log", true))
            {
                file.WriteLine(DateTime.Now.ToString() + ": sender=" + sender.ToString() + " Exception=" + ex.Message);
            }
        }
    }
}
