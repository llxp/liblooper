﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Resources;
using System.Windows;

namespace LibLooper.Utils
{
    public class SoundManager : IDisposable
    {
        private readonly object m_locker = new object();
        private Queue<string> m_queue = new Queue<string>();
        private EventWaitHandle m_wh = new AutoResetEvent(false);
        private Thread m_soundThread;
        public SoundManager()
        {
            m_soundThread = new Thread(threadLoop);
            m_soundThread.Start();
        }

        private void threadLoop()
        {
            while(true)
            {
                string soundFile = null;
                lock(m_locker)
                {
                    if(m_queue.Count > 0)
                    {
                        soundFile = m_queue.Dequeue();
                        if(soundFile == null)
                        {
                            return;
                        }
                    }
                }
                if(soundFile != null)
                {
                    playSoundFile(soundFile);
                }
                else
                {
                    m_wh.WaitOne();
                }
            }
        }

        /*
         * this method will play a sound file
         */
        private void playSoundFile(string soundFile)
        {
            try
            {
                using (var player = new SoundPlayer(soundFile))
                {
                    player.PlaySync();
                }
            }
            catch(Exception ex)
            {
                Logger.logExceptionToFile("SoundManager.playSoundFile", ex);
            }
        }

        /*
         * this method will queue a sound file
         */
        public void PlaySoundFile(string soundFile)
        {
            lock(m_locker)
            {
                m_queue.Enqueue(soundFile);
            }
            m_wh.Set();
        }

        public void Dispose()
        {
            PlaySoundFile(null);     // Signal the consumer to exit.
            m_soundThread.Join();         // Wait for the consumer's thread to finish.
            m_wh.Close();            // Release any OS resources.
        }
    }
}
