﻿using LibLooper.LooperJobs.RestModels;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibLooper.Utils
{
    internal static class LooperBacklogManagerUtils
    {
        public static LooperData PopulateIcMLooperData(Incident incident)
        {
            LooperData looperData = new LooperData();
            looperData.Id = incident.Id.ToString();
            looperData.Title = incident.Title;
            looperData.Type = incident.Type;
            looperData.Source = "icm";
            looperData.SourceItem = incident;
            looperData.State = incident.State;
            looperData.Severity = incident.Severity.ToString();
            return looperData;
        }

        public static LooperData PopulateTfsLooperData(WorkItem child)
        {
            LooperData newData = new LooperData();
            newData.Id = child.Id.ToString();
            newData.Title = child.Title;
            newData.Type = child.Type.Name;
            newData.Source = "tfs";
            newData.SourceItem = child;
            newData.SourceItem = child;
            return newData;
        }

        public static bool CheckSeverityField(WorkItem child) => child.Fields != null && child.Fields.Contains("Severity") && child.Fields["Severity"] != null;
        public static bool CheckEventTimeField(WorkItem child) => child.Fields != null && child.Fields.Contains("Event Time") && child.Fields["Event Time"] != null && child.Fields["Event Time"].Value != null;

        public  delegate void SevHandlerCalback(Severity severityLevel);
        public static bool CheckSeverityLevels(List<Severity> severityList)
        {
            if (severityList != null)
            {
                return true;
            }
            return false;
        }

        public static void HandleSevLevelsIcM(List<Severity> severityList, SevHandlerCalback callback, Incident incident)
        {
            if (CheckSeverityLevels(severityList))
            {
                foreach (Severity severity in severityList)
                {
                    if (severity.severity == incident.Severity)
                    {
                        callback(severity);
                        break;
                    }
                }
            }
            callback(null);
        }

        public static void HandleSevLevelsTfs(List<Severity> severityList, LooperBacklogManagerUtils.SevHandlerCalback callback, WorkItem workItem)
        {
            if (CheckSeverityLevels(severityList))
            {
                if (CheckSeverityField(workItem))
                {
                    int sev = -1;
                    if (int.TryParse(workItem.Fields["Severity"].Value.ToString(), out sev))
                    {
                        foreach (Severity severity in severityList)
                        {
                            if (sev != -1 && severity.severity == sev)
                            {
                                callback(severity);
                                break;
                            }
                        }
                    }
                }
                else
                {
                    foreach (Severity severity in severityList)
                    {
                        if (severity.severity == -1)
                        {
                            callback(severity);
                            break;
                        }
                    }
                }
            }
            callback(null);
        }
    }
}
