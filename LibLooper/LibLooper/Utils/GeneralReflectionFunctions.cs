﻿using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LibLooper.Utils
{
    /*
     * This class contains some useful reflection functions used in the query parser
     */
    sealed class GeneralReflectionFunctions
    {
        public static object GetClass(Assembly assembly, string type)
        {
            if (assembly == null || type == null || (type != null && type.Length <= 0))
            {
                return null;
            }
            return assembly.GetType();
        }

        public static bool HasProperty(object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName) != null;
        }

        public static object GetPropertyValue(object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName).GetValue(obj, null);
        }

        public static bool HasVariable(object obj, string variableName)
        {
            return obj.GetType().GetField(variableName).GetValue(obj) != null;
        }

        public static object GetVariable(object obj, string variableName)
        {
            return obj.GetType().GetField(variableName).GetValue(obj);
        }

        public static bool HasMethod(object obj, string methodName, Type[] signature)
        {
            return obj.GetType().GetMethod(methodName, signature) != null;
        }

        public static object ExecuteMethod(object obj, string methodName, List<object> parameter)
        {
            Type t = obj.GetType();
            object output = new object();
            object retVal = null;
            Type[] signature = null;
            List<Type> sigs = new List<Type>();
            List<object> parameters = new List<object>();
            foreach (var currParam in parameter)
            {
                sigs.Add(currParam.GetType());
                parameters.Add(currParam);
            }
            signature = sigs.ToArray();
            if (parameters.Count > 0)
            {
                if (methodName != "root")
                {
                    try
                    {
                        retVal = t.GetMethod(methodName, signature).Invoke(obj, parameters.ToArray());
                    }
                    catch
                    {
                        //Logger.Write(ex.StackTrace, "Exception");
                    }
                }
                else
                {
                    string retStr = "";
                    foreach (var str in parameter)
                    {
                        if (str.GetType() == typeof(string))
                        {
                            retStr += str + ",";
                        }
                    }
                    if (retStr.Length > 0 && retStr[retStr.Length - 1] == ',')
                    {
                        retStr = retStr.Substring(0, retStr.Length - 1);
                    }
                    retVal = retStr;
                }
            }
            else
            {
                if (methodName != "root")
                {
                    try
                    {
                        retVal = t.GetMethod(methodName, signature).Invoke(obj, null);
                    }
                    catch
                    {
                        //Logger.Write(ex.StackTrace, "Exception");
                    }
                }
                else
                {
                    string retStr = "";
                    foreach (var str in parameter)
                    {
                        if (str.GetType() == typeof(string))
                        {
                            retStr += str + ",";
                        }
                    }
                    if (retStr[retStr.Length - 1] == ',')
                    {
                        retStr = retStr.Substring(0, retStr.Length - 1);
                    }
                    retVal = retStr;
                    //return new KeyValuePair<Type, object>(typeof(List<KeyValuePair<Type, object>>), parameter);
                }
            }
            return retVal;
        }

        public static List<Assembly> GetAllLoadedAssemblies()
        {
            List<Assembly> output = new List<Assembly>();
            foreach (AssemblyName an in Assembly.GetExecutingAssembly().GetReferencedAssemblies())
            {
                Assembly asm = Assembly.Load(an.ToString());
                output.Add(asm);
            }
            return output;
        }

        public static Assembly CompileScript(string scriptFile)
        {
            FileStream fs = new FileStream(scriptFile, FileMode.Open);
            StreamReader sr = new StreamReader(fs);
            string srcCode = sr.ReadToEnd();
            CSharpCodeProvider provider = new CSharpCodeProvider();
            CompilerParameters parameters = new CompilerParameters();
            List<Assembly> referencedAssemblies = GetAllLoadedAssemblies();
            foreach (var assembly in referencedAssemblies)
            {
                if (assembly != null)
                {
                    parameters.ReferencedAssemblies.Add(assembly.GetName().FullName);
                }
            }
            parameters.GenerateInMemory = true;
            parameters.GenerateExecutable = false;
            CompilerResults results = provider.CompileAssemblyFromSource(parameters, srcCode);
            if (results.Errors.HasErrors)
            {
                StringBuilder sb = new StringBuilder();

                foreach (CompilerError error in results.Errors)
                {
                    sb.AppendLine(String.Format("Error ({0}): {1}", error.ErrorNumber, error.ErrorText));
                }
                throw new InvalidOperationException(sb.ToString());
            }
            return results.CompiledAssembly;
        }
    }
}
