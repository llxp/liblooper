﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace LibLooper.Utils
{
    public class CertManager : Logger
    {
        private static X509Certificate2 GetCert(StoreLocation location)
        {
            X509Certificate2 result = null;
            X509Store certStore = new X509Store(StoreName.My, location);
            try
            {
                certStore.Open(OpenFlags.ReadOnly);
            }
            catch (Exception ex)
            {
                logExceptionToFile("CertManager.GetCert", ex);
            }

            try
            {
                X509Certificate2Collection certs = X509Certificate2UI.SelectFromCollection(certStore.Certificates, "Select Certificate", "Select Certificate", X509SelectionFlag.SingleSelection);
                if (certs.Count > 0)
                {
                    result = certs[0];
                }
            }
            catch (Exception ex)
            {
                logExceptionToFile("CertManager.GetCert", ex);
            }
            finally
            {
                certStore.Close();
            }

            return result;
        }

        /// <summary>Gets the cert</summary>
        /// <returns>resulting value</returns>
        public static X509Certificate2 GetCert()
        {
            return GetCert(StoreLocation.CurrentUser) ??
                   GetCert(StoreLocation.LocalMachine);
        }
    }
}
