﻿using LibLooper.IcMAuthentication.RestModels;
using LibLooper.LooperJobs;
using LibLooper.Utils;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;

namespace LibLooper
{
    internal class LooperThread {
        public delegate void AccessTokenEventHandler(LooperThread looper);

        private event LooperJob.GotDataEventHandler m_gotData;
        //private event TfsLooperJob.TfsLooperJobEventHandler m_tfsLooperGotData;
        //private event IcMLooperJob.IcMLooperJobEventHandler m_icmLooperGotdata;

        private System.Timers.Timer m_managementTimer = new System.Timers.Timer();

        public double TickTime { get; set; } = 30.0;
        public LooperContainer Loopers { get; private set; } = new LooperContainer();

        public event /*TfsLooperJob.TfsLooperJobEventHandler*/LooperJob.GotDataEventHandler GotData {
            add {
                m_gotData += value;
                //m_tfsLooperGotData += value;
                foreach (LooperJob job in Loopers) {
                    if (job.Type == LooperType.Tfs) {
                        ((TfsLooperJob)job).GotData += value;
                    }
                }
            }
            remove {
                m_gotData += value;
                //m_tfsLooperGotData -= value;
                foreach (LooperJob job in Loopers) {
                    if (job.Type == LooperType.Tfs) {
                        ((TfsLooperJob)job).GotData -= value;
                    }
                }
            }
        }

        /*public event IcMLooperJob.IcMLooperJobEventHandler IcMLooperGotData {
            add {
                m_icmLooperGotdata += value;
                foreach (LooperJob job in Loopers) {
                    if (job.Type == LooperType.IcM) {
                        ((IcMLooperJob)job).GotData += value;
                    }
                }
            }
            remove {
                m_icmLooperGotdata -= value;
                foreach (LooperJob job in Loopers) {
                    if (job.Type == LooperType.IcM) {
                        ((IcMLooperJob)job).GotData -= value;
                    }
                }
            }
        }*/

        /*private void addJob<T>(T jobEvent, LooperType type) {
            foreach (LooperJob job in Loopers) {
                if (job.Type == type) {
                    ((JobType)job).GotData += jobEvent;
                }
            }
        }

        private void removeJob<T>(T jobEvent, LooperType type) where T: TfsLooperJob.TfsLooperJobEventHandler {
            foreach (LooperJob job in Loopers) {
                if (job.Type == LooperType.IcM) {
                    ((JobType)job).GotData += job;
                }
            }
        }*/

        public void InitLooperThread()
        {
            m_managementTimer = new System.Timers.Timer {
                Interval = TimeSpan.FromSeconds(TickTime).TotalMilliseconds
            };
            m_managementTimer.Elapsed += ManagementTimer_Elapsed;
        }

        private static LooperJob createTfsJob(string name, LooperConfig entry)
        {
            return new TfsLooperJob()
            {
                JobName = name,
                Query = entry.Query,
                PreviousQuery = entry.PreviousQuery,
                Config = entry,
                ShouldRun = entry.DefaultRunning,
            };
        }

        private static LooperJob createIcMJob(string name, LooperConfig entry)
        {
            return new IcMLooperJob()
            {
                JobName = name,
                Query = entry.Query,
                PreviousQuery = entry.PreviousQuery,
                Config = entry,
                ShouldRun = entry.DefaultRunning
            };
        }

        public bool AddQuery(LooperConfig entry, string name)
        {
            if(Loopers.Any(a=>a.JobName == name))
            {
                return false;
            }
            switch (entry.Source.ToLowerInvariant())
            {
                case "tfs":
                    LooperJob newTfsJob = createTfsJob(name, entry);
                    //newTfsJob.GotData += m_tfsLooperGotData;
                    newTfsJob.GotData += m_gotData;
                    Loopers.Add(newTfsJob);
                    break;
                case "icm":
                    LooperJob newIcMJob = createIcMJob(name, entry);
                    newIcMJob.GotData += m_gotData;
                    //newIcMJob.GotData += m_icmLooperGotdata;
                    Loopers.Add(newIcMJob);
                    break;
                default:
                    break;
            }
            return true;
        }

        /*
         * this method runs all added queries/loopers
         * this method will be triggered, when the timer gets a timeout
         */
        private void ManagementTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.m_managementTimer.Stop();
            CountdownEvent resetEvent = new CountdownEvent(Loopers.Count);
            foreach (var job in this.Loopers)
            {
                ThreadPool.QueueUserWorkItem(state =>
                {
                    job.Start();
                    resetEvent.Signal();
                });
            }
            resetEvent.Wait();
            this.m_managementTimer.Start();
        }

        /*
         * cancels all loopers/jobs
         */
        public void CancelAll()
        {
            m_managementTimer.Stop();
        }

        /*
         * starts the timer and all jobs which are set to 'ShouldRun'
         */
        public void StartAll()
        {
            m_managementTimer.Start();
        }
    }
}
