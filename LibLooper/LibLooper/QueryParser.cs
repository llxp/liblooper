﻿using LibLooper.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LibLooper
{
    public class QueryParser
    {
        public static object addons_assembly = null;
        public QueryParser()
        {
            if (addons_assembly == null)
            {
                /*
                 * Load the addons, either precompiled or compiled during the runtime.
                 */
                addons_assembly = new addons();
                //if (File.Exists("addons.cs"))
                //{
                //addons_assembly = GeneralReflectionFunctions.CompileScript("addons.cs");
                //}
            }
        }

        /*This function checks, if all brackets are opened and also closed
         */
        private bool PreCheckstring(string str)
        {
            int ch1 = 0, ch2 = 0;
            foreach (char ch in str)
            {
                if (ch == '(')
                {
                    ch1++;
                }
                if (ch == ')')
                {
                    ch2++;
                }
            }
            if (ch1 == ch2)
            {
                return true;
            }
            return false;
        }

        /*
         * This function gets all outer queries and returns them with the start and end positions inside the string
         */
        public Dictionary<KeyValuePair<int, int>, string> getOuterQuery(string query)
        {
            Dictionary<KeyValuePair<int, int>, string> output = new Dictionary<KeyValuePair<int, int>, string>();
            if (PreCheckstring(query))
            {
                List<int> positions = new List<int>();
                for (int i = query.IndexOf('$'); i >= 0; i = query.IndexOf('$', i + 1))
                {
                    positions.Add(i);
                }
                foreach (var pos1 in positions)
                {
                    if (pos1 != -1)
                    {
                        if (query[pos1 + 1] == '(')
                        {
                            string str_ = query.Substring(pos1 + 1);
                            int ch_1 = 0, ch_2 = 0;
                            int ch_3 = ch_2 - ch_1;
                            int pos2 = 0;
                            for (int i = 0; i < str_.Length; i++)
                            {
                                if (str_[i] == '(')
                                {
                                    ch_3++;
                                }
                                if (str_[i] == ')')
                                {
                                    ch_3--;
                                }
                                if (ch_3 == 0)
                                {
                                    pos2 = i + pos1 - 1;
                                    break;
                                }
                            }
                            if (pos2 - pos1 > 0)
                            {
                                string queryable_str = query.Substring(pos1 + 2, pos2 - pos1);
                                output.Add(new KeyValuePair<int, int>(pos1, pos2), queryable_str);
                            }
                        }
                    }
                }
            }
            return output;
        }

        /*
         * This function can be used to parse a query
         */
        public string ParseQuery(string query)
        {
            if (addons_assembly == null)
            {
                return "";
            }
            Dictionary<KeyValuePair<int, int>, string> allQueries = getOuterQuery(query);
            while (allQueries.Count > 0)
            {
                if (allQueries.Count == 0)
                {
                    break;
                }
                KeyValuePair<KeyValuePair<int, int>, string> currQuery = allQueries.ToArray()[0];
                processedResult preResult = new processedResult() { FunctionName = "root", InnerLevels = null, InnerQuery = currQuery.Value };
                preResult = processInnerLevelWithName(preResult);
                object result = processFunctionTree(preResult);
                string str = "";
                if (result.GetType() == typeof(string))
                {
                    str = result as string;
                }
                string str1 = query.Substring(0, currQuery.Key.Key);
                string str2 = query.Substring(currQuery.Key.Value + 3, query.Length - (currQuery.Key.Value + 3));
                query = str1 + str + str2;
                allQueries = getOuterQuery(query);
            }
            return query;
        }

        /*
         * This class is used for handling the recursion.
         */
        private class processedResult
        {
            public processedResult()
            {

            }

            public Type ReturnType { get; set; } = null;

            public object ReturnValue { get; set; } = null;

            public string FunctionName { get; set; } = "";

            public string InnerQuery { get; set; } = "";

            public List<processedResult> InnerLevels { get; set; } = null;
        }

        /*
         * This function handles the functions and recursively gets all inner functions or data used as a parameter
         * actual only one paramater, either data or a function is possible
         */
        private processedResult processInnerLevelWithName(processedResult input)
        {
            List<processedResult> innerResults = new List<processedResult>();
            input.InnerLevels = new List<processedResult>();
            innerResults = new List<processedResult>();
            if (PreCheckstring(input.InnerQuery))
            {
                int fPos = input.InnerQuery.IndexOf("(");
                if (fPos != -1)
                {
                    List<string> funcQueries = new List<string>();
                    List<string> funcNames = new List<string>();
                    int count = input.InnerQuery.Length;
                    int ch = 0;
                    bool started = false;
                    int startPos = 0;
                    for (int i = 0; i < count; i++)
                    {
                        if (input.InnerQuery[i] == '(')
                        {
                            if (ch >= 0)
                            {
                                if (ch == 0)
                                {
                                    startPos = i;
                                    started = true;
                                }
                                ch++;
                            }
                        }
                        if (input.InnerQuery[i] == ')')
                        {
                            ch--;
                        }
                        if (started == true && ch == 0)
                        {
                            started = false;
                            string fName = "";
                            if (i > 0)
                            {
                                for (int x = startPos; x >= 0; x--)
                                {
                                    int ch_x = input.InnerQuery[x];
                                    if ((ch_x >= 48 && ch_x <= 57) || (ch_x >= 65 && ch_x <= 90) || (ch_x >= 97 && ch_x <= 122) || ch_x == (int)'_')
                                    {
                                        fName += input.InnerQuery[x];
                                    }
                                }
                            }
                            char[] arr = fName.ToArray();
                            Array.Reverse(arr);
                            fName = new string(arr);
                            funcNames.Add(fName);
                            funcQueries.Add(input.InnerQuery.Substring((startPos + 1), i - (startPos + 1)));
                            startPos = 0;
                        }
                    }
                    for (int i = 0; i < funcQueries.Count; i++)
                    {
                        string innerQuery = funcQueries[i];
                        string FunctionName = funcNames[i];
                        processedResult result = new processedResult()
                        {
                            FunctionName = FunctionName,
                            InnerLevels = null,
                            InnerQuery = innerQuery
                        };
                        result = processInnerLevelWithName(result);
                        innerResults.Add(result);
                    }
                }
            }
            input.InnerLevels.AddRange(innerResults);
            return input;
        }
        /*
         * This function gets the previously created inputTree as an input and gets the results from the methods
         */
        private object processFunctionTree(processedResult inputTree)
        {
            if (inputTree.InnerLevels != null && inputTree.InnerLevels.Count > 0)
            {
                List<object> retVals = new List<object>();
                foreach (processedResult currentLevel in inputTree.InnerLevels)
                {
                    object retVal = processFunctionTree(currentLevel);
                    retVals.Add(retVal);
                }
                return GeneralReflectionFunctions.ExecuteMethod(addons_assembly, inputTree.FunctionName, retVals);
            }
            return GeneralReflectionFunctions.ExecuteMethod(addons_assembly, inputTree.FunctionName, new List<object>() { inputTree.InnerQuery });
        }
    }
}