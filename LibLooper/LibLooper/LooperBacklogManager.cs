﻿using LibLooper.IcMAuthentication.RestModels;
using LibLooper.LooperJobs;
using LibLooper.LooperJobs.RestModels;
using LibLooper.Utils;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Threading;
using System.Timers;
using System.Windows;
using static LibLooper.LooperJobs.TfsReader;
using static LibLooper.LooperThread;

namespace LibLooper
{
    public class LooperBacklogManager
    {
        private LooperThread m_looperThread = new LooperThread();
        private List<LooperData> IgnoreList { get; set; } = new List<LooperData>();
        public bool PlaySound { get; set; } = false;

        public delegate void NewTicketEventHandler(LooperData ticket);
        public event NewTicketEventHandler NewTicket;

        public double TickTime
        {
            set
            {
                m_looperThread.TickTime = value;
            }
            get
            {
                return m_looperThread.TickTime;
            }
        }

        public void InitLooperBacklogManager()
        {
            //m_looperThread.TfsLooperGotData += NewTfsJob_GotData;
            //m_looperThread.IcMLooperGotData += NewIcMJob_GotData;
            m_looperThread.GotData += looperJob_GotData;
            m_looperThread.InitLooperThread();
        }

        public void ResumeJob(string job)
        {
            m_looperThread.Loopers.ResumeJob(job);
        }

        public void CancelAll()
        {
            m_looperThread.CancelAll();
        }

        public void StartAll()
        {
            m_looperThread.StartAll();
        }

        public void PauseAll()
        {
            m_looperThread.Loopers.PauseAll();
        }

        public void PauseJob(string job)
        {
            m_looperThread.Loopers.PauseJob(job);
        }

        public bool AddQuery(LooperConfig config, string name)
        {
            return m_looperThread.AddQuery(config, name);
        }

        private bool checkIcMIgnoreList(Incident incident)
        {
            if (IgnoreList.Any(obj => (
                ((Incident)obj.SourceItem).AcknowledgeBy == incident.AcknowledgeBy
                && ((Incident)obj.SourceItem).AssignedTo == incident.AssignedTo
                && ((Incident)obj.SourceItem).CreatedDate == incident.CreatedDate
                && ((Incident)obj.SourceItem).IsAcknowledged == incident.IsAcknowledged
                && ((Incident)obj.SourceItem).OwningServiceId == incident.OwningServiceId
                && ((Incident)obj.SourceItem).OwningTeamId == incident.OwningTeamId
                && ((Incident)obj.SourceItem).Severity == incident.Severity
                && ((Incident)obj.SourceItem).State == incident.State
                && ((Incident)obj.SourceItem).Title == incident.Title
                && ((Incident)obj.SourceItem).Type == incident.Type
            )))
            {
                return true;
            }
            return false;
        }

        private bool checkTfsIgnoreList(WorkItem workItem)
        {
            if (IgnoreList.Any(obj =>
                ((WorkItem)obj.SourceItem).ChangedDate == workItem.ChangedDate
                && ((WorkItem)obj.SourceItem).ChangedBy == workItem.ChangedBy
                && ((WorkItem)obj.SourceItem).CreatedDate == workItem.CreatedDate
                && ((WorkItem)obj.SourceItem).CreatedBy == workItem.CreatedBy
                && ((WorkItem)obj.SourceItem).Description == workItem.Description
                && ((WorkItem)obj.SourceItem).DisplayForm == workItem.DisplayForm
                && (
                    ((WorkItem)obj.SourceItem).Fields != null
                    && ((WorkItem)obj.SourceItem).Fields.Contains("Severity")
                    && workItem.Fields.Contains("Severity")
                    && ((WorkItem)obj.SourceItem).Fields["Severity"] == workItem.Fields["Severity"])
                && ((WorkItem)obj.SourceItem).History == workItem.History
                && ((WorkItem)obj.SourceItem).HyperLinkCount == workItem.HyperLinkCount
                && ((WorkItem)obj.SourceItem).NodeName == workItem.NodeName
                && ((WorkItem)obj.SourceItem).State == workItem.State
                && ((WorkItem)obj.SourceItem).Title == workItem.Title
                && ((WorkItem)obj.SourceItem).Type == workItem.Type
                && ((WorkItem)obj.SourceItem).WorkItemLinks == workItem.WorkItemLinks
                && ((WorkItem)obj.SourceItem).WorkItemLinkHistory == workItem.WorkItemLinkHistory))
            {
                return true;
            }
            return false;
        }

        private void looperJob_GotData(LooperJob sender, List<object> e) {
            switch (sender.Type) {
                case LooperType.IcM:
                    newIcMJob_GotData((IcMLooperJob)sender, (List<Incident>)e.Cast<Incident>());
                    break;
                case LooperType.Tfs:
                    newTfsJob_GotData((TfsLooperJob)sender, (List<WorkItem>)e.Cast<WorkItem>());
                    break;
                default:
                    break;
            }
        }

        /*
         * Normal icm method, which will be triggered, when something from icm comes in
         */
        private void newIcMJob_GotData(IcMLooperJob sender, IEnumerable<Incident> e)
        {
            foreach (Incident incident in e)
            {
                if (incident != null)
                {
                    if (checkIcMIgnoreList(incident))
                    {
                        continue;
                    }

                    LooperBacklogManagerUtils.HandleSevLevelsIcM(sender.Config.SlaTimes, (Severity confSev) => {
                        try
                        {
                            if (confSev != null
                                && !incident.IsAcknowledged
                                && (DateTime.Now - incident.CreatedDate) <= (DateTime.Now - DateTime.Now.AddMinutes(confSev.time)))
                            {
                                LooperData newData = LooperBacklogManagerUtils.PopulateIcMLooperData(incident);
                                newData.LooperName = sender.JobName;
                                newData.Config = sender.Config;
                                IgnoreList.Add(newData);
                                NewTicket(newData);
                            }
                            else if (confSev == null)
                            {
                                LooperData newData = LooperBacklogManagerUtils.PopulateIcMLooperData(incident);
                                newData.LooperName = sender.JobName;
                                newData.Config = sender.Config;
                                IgnoreList.Add(newData);
                                NewTicket(newData);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.logExceptionToFile("LooperThread.NewIcMJob_GotData", ex);
                            LooperData newData = LooperBacklogManagerUtils.PopulateIcMLooperData(incident);
                            newData.LooperName = sender.JobName;
                            newData.Config = sender.Config;
                            IgnoreList.Add(newData);
                            NewTicket(newData);
                        }
                    }, incident);
                }
            }
        }

        /*
         * when something from tfs comes in, this method will be triggered
         */
        private void newTfsJob_GotData(TfsLooperJob sender, List<WorkItem> e)
        {
            foreach (WorkItem child in e)
            {
                if (child != null)
                {
                    if (checkTfsIgnoreList(child))
                    {
                        continue;
                    }

                    DateTime startTime = DateTime.Parse("00:00:00");
                    if (LooperBacklogManagerUtils.CheckEventTimeField(child))
                    {
                        startTime = (DateTime)child.Fields["Event Time"].Value;
                    }
                    else
                    {
                        startTime = child.CreatedDate;
                    }
                    LooperBacklogManagerUtils.HandleSevLevelsTfs(sender.Config.SlaTimes, (Severity confSev) => {
                        if (confSev != null && (DateTime.Now - startTime) >= (DateTime.Now - DateTime.Now.AddMinutes(confSev.showTime)))
                        {
                            LooperData newData = LooperBacklogManagerUtils.PopulateTfsLooperData(child);
                            if (LooperBacklogManagerUtils.CheckEventTimeField(child))
                            {
                                TimeSpan timeDiff = (DateTime.Now - ((DateTime)child.Fields["Event Time"].Value));
                                if (timeDiff >= (DateTime.Now - DateTime.Now.AddMinutes(confSev.warningTime)))
                                {
                                    newData.WarningLevel = 2;
                                }
                                else if (timeDiff >= (DateTime.Now - DateTime.Now.AddMinutes(confSev.time)))
                                {
                                    newData.WarningLevel = 1;
                                }
                            }
                            newData.LooperName = sender.JobName;
                            newData.Config = sender.Config;
                            if (LooperBacklogManagerUtils.CheckSeverityField(child))
                            {
                                newData.Severity = child.Fields["Severity"].ToString();
                            }
                            IgnoreList.Add(newData);
                            NewTicket(newData);
                        }
                        else if (confSev == null)
                        {
                            LooperData newData = LooperBacklogManagerUtils.PopulateTfsLooperData(child);
                            newData.LooperName = sender.JobName;
                            newData.Config = sender.Config;
                            if (LooperBacklogManagerUtils.CheckSeverityField(child))
                            {
                                string severity = child.Fields["Severity"].Value.ToString();
                                newData.Severity = severity;
                            }
                            IgnoreList.Add(newData);
                            NewTicket(newData);
                        }
                    }, child);
                }
            }
        }
    }
}