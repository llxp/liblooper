﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibLooper
{
    public class LooperData
    {
        public string Title { get; set; } = "";
        public string Id { get; set; } = "";
        public string Type { get; set; } = "";
        public string Source { get; set; } = "";
        public string LooperName { get; set; } = "";
        public object SourceItem { get; set; }
        public int WarningLevel { get; set; } = 0;
        public string State { get; set; } = "";
        public LooperConfig Config { get; set; } = new LooperConfig();
        public string Description { get; set; } = "";
        public string History { get; set; } = "";
        public string Severity { get; set; } = "";
    }
}