﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace LibLooper.LooperJobs.RestModels
{
    [DataContract]
    internal class IncidentList
    {
        [DataMember(Name = "@odata.context")]
        public string context { get; set; }
        [DataMember(Name = "value")]
        public List<Incident> value { get; set; }
    }

    [DataContract]
    internal class Incident
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int Severity { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public long OwningServiceId { get; set; }
        [DataMember]
        public long OwningTeamId { get; set; }
        [DataMember]
        public long AssignedTo { get; set; }
        [DataMember]
        public string NotificationStatus { get; set; }
        [DataMember]
        public long HitCount { get; set; }
        [DataMember]
        public long ChildCount { get; set; }
        [DataMember]
        public long AcknowledgeBy { get; set; }
        [DataMember]
        public object ParentId { get; set; }
        [DataMember]
        public long ExternalLinksCount { get; set; }
        [DataMember]
        public bool IsAcknowledged { get; set; }
        [DataMember]
        public string Type { get; set; }
    }
}
