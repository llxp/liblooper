﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using LibLooper.Utils;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using LibLooper.LooperJobs;
using LibLooper.LooperJobs.RestModels;
using LibLooper.IcMAuthentication.RestModels;
using System.Runtime.InteropServices;
using System.Threading;
using System.Collections;

namespace LibLooper.LooperJobs
{
    internal class IcMReader : Reader<IcMReader>, IReader
    {
        private const int INTERNET_OPTION_END_BROWSER_SESSION = 42;
        [DllImport("wininet.dll", SetLastError = true)]
        private static extern bool InternetSetOption(IntPtr hInternet, int dwOption, IntPtr lpBuffer, int lpdwBufferLength);

        private IcMToken m_bearerToken = null;

        public IcMReader() {}

        public override IEnumerable<T> RunQuery<T>(string query) {
            // build the URL we'll hit
            string url = string.Format("{0}/api2/incidentapi/incidents?$filter={1}", m_baseUrl, query);

            if (query.Contains("$select=")) {
                url = HttpUtils.RemoveQueryStringByKey(url, "$select");
            }

            url = string.Format("{0}&$select=Id,Severity,State,Title,CreatedDate,OwningServiceId,OwningTeamId,AssignedTo,NotificationStatus,HitCount,ChildCount,AcknowledgeBy,ParentId,RootCause,ExternalLinksCount,CustomFields,IsAcknowledged,Type", url);

            // create the request
            HttpWebRequest req = WebRequest.CreateHttp(url);
            req.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + m_bearerToken.access_token);

            // submit the request
            try {
                // read out the response stream as text
                string json = HttpUtils.GetResponseFromRequest(req);
                // deserialize it into an incident object
                IncidentList incidentList = HttpUtils.DeSerializeResponse<IncidentList>(json);
                return (IEnumerable<T>)incidentList.value;
            } catch (Exception ex) {
                Logger.logExceptionToFile("IcMReader.QueryIncidents", ex);
            }

            return null;
        }

        protected override void init() {
            if (m_bearerToken == null || (m_bearerToken != null && (DateTime.Now - m_bearerToken.ObjectCreatedAt).Minutes > m_bearerToken.expires_in)) {
                AutoResetEvent waitHandle = new AutoResetEvent(false);
                Thread t = new Thread(() => {
                    bool tokenSet = false;
                    InternetSetOption(IntPtr.Zero, INTERNET_OPTION_END_BROWSER_SESSION, IntPtr.Zero, 0);
                    IcMAuthenticationForm authenticationForm = new IcMAuthenticationForm();
                    authenticationForm.GotToken += (IcMToken token) => {
                        authenticationForm.Close();
                        m_bearerToken = token;
                        tokenSet = true;
                        waitHandle.Set();
                    };
                    authenticationForm.FormClosed += (object sender, FormClosedEventArgs e) => {
                        if (tokenSet == false && (e.CloseReason == CloseReason.ApplicationExitCall || e.CloseReason == CloseReason.TaskManagerClosing || e.CloseReason == CloseReason.UserClosing)) {
                            m_bearerToken = null;
                        }
                        waitHandle.Set();
                    };
                    authenticationForm.ShowDialog();
                });
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
                waitHandle.WaitOne();
            }
        }
    }
}
