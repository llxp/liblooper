﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibLooper.LooperJobs
{
    internal class LooperContainer : IEnumerable<LooperJob>
    {
        private List<LooperJob> m_jobs = new List<LooperJob>();

        /*
         * pauses all running loopers/jobs
         */
        public void PauseAll()
        {
            foreach (var job in m_jobs)
            {
                job.ShouldRun = false;
            }
        }

        /*
         * resumes all paused/not running loopers/jobs
         */
        public void ResumeAll()
        {
            foreach (var job in m_jobs)
            {
                job.ShouldRun = true;
            }
        }

        /*
         * resumes the specified job/looper
         */
        public void ResumeJob(string jobName)
        {
            foreach (var job in m_jobs)
            {
                if (job.JobName == jobName)
                {
                    job.ShouldRun = true;
                    return;
                }
            }
        }

        /*
         * pauses the specified job/looper
         */
        public void PauseJob(string jobName)
        {
            foreach (var job in m_jobs)
            {
                if (job.JobName == jobName)
                {
                    job.ShouldRun = false;
                    return;
                }
            }
        }

        public void Add(LooperJob job)
        {
            this.m_jobs.Add(job);
        }

        public void RunAllLoopers()
        {

        }

        public IEnumerator<LooperJob> GetEnumerator()
        {
            return m_jobs.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public int Count { get { return m_jobs.Count; } }
    }
}
