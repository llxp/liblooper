﻿using LibLooper.LooperJobs;
using LibLooper.LooperJobs.RestModels;
using LibLooper.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibLooper
{
    internal class IcMLooperJob : LooperJob
    {
        public IcMLooperJob()
        {
        }
        //public delegate void IcMLooperJobEventHandler(IcMLooperJob sender, List<Incident> e);
        //public event IcMLooperJobEventHandler GotData;

        public override void Start()
        {
            try
            {
                IReader reader = IcMReader.Instance(Properties.Settings.Default.IcMEndpoint);
                List<Incident> incidents = reader.RunQuery<Incident>(Query).ToList();
                if (incidents != null && incidents.Count > 0)
                {
                    SendData(this, (List<object>)(object)incidents);
                }
            }
            catch (Exception ex)
            {
                Logger.logExceptionToFile("IcMLooperJob.Start", ex);
            }
        }
    }
}
