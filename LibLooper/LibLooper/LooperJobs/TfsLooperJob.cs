﻿using LibLooper.Utils;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace LibLooper.LooperJobs
{
    internal class TfsLooperJob : LooperJob
    {
        public TfsLooperJob()
        {
            Type = LooperType.Tfs;
        }

        //public delegate void TfsLooperJobEventHandler(TfsLooperJob sender, List<WorkItem> e);
        //public event TfsLooperJobEventHandler GotData;

        public override void Start()
        {
            try
            {
                IReader reader = TfsReader.Instance(Properties.Settings.Default.TfsEndpoint);
                string currentQuery = Query;
                if (PreviousQuery != null)
                {
                    List<WorkItem> check = reader.RunQuery<WorkItem>(PreviousQuery.CheckQuery).ToList();
                    if (check != null && check.Count == 1)
                    {
                        if (check[0].State == PreviousQuery.CheckState)
                        {
                            currentQuery = PreviousQuery.AlternateQuery;
                        }
                    }
                }
                List<WorkItem> escorts = reader.RunQuery<WorkItem>(currentQuery).ToList();
                if (escorts != null && escorts.Count > 0)
                {
                    SendData(this, (List<object>)(object)escorts);
                }
            }
            catch (Exception ex)
            {
                Logger.logExceptionToFile("TfsLooperJob.Start", ex);
            }
        }
    }
}