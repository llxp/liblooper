﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibLooper.LooperJobs
{
    public interface IReader
    {
        IEnumerable<T2> RunQuery<T2>(string query);
    }
    public abstract class Reader<T> : IReader where T : Reader<T>, IReader, new()
    {
        public Reader()
        {
        }
        public abstract IEnumerable<T2> RunQuery<T2>(string query);
        protected abstract void init();

        private static Dictionary<string, Reader<T>> m_instance = new Dictionary<string, Reader<T>>();
        private static readonly object m_locker = new object();

        protected string m_baseUrl = "";

        public static Reader<T> Instance(string baseUrl)
        {
            if (!m_instance.ContainsKey(baseUrl))
            {
                lock (m_locker)
                {
                    if (!m_instance.ContainsKey(baseUrl))
                    {
                        m_instance.Add(baseUrl, new T());
                        m_instance[baseUrl].m_baseUrl = baseUrl;
                    }
                }
            }
            lock (m_locker)
            {
                if (m_instance.ContainsKey(baseUrl))
                {
                    m_instance[baseUrl].init();
                }
            }
            return m_instance[baseUrl];
        }
    }
}
