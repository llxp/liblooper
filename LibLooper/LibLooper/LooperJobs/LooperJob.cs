﻿using LibLooper.Utils;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace LibLooper.LooperJobs
{
    internal enum LooperType {IcM, Tfs}
    internal class LooperJob {
        public delegate void GotDataEventHandler(LooperJob sender, List<object> items);
        public event GotDataEventHandler GotData;
        protected void SendData(LooperJob sender, List<object> items) {
            GotData?.Invoke(sender, items);
        }
        public string m_baseUrl { get; set; }

        public LooperType Type { get; set; }
        public object Tag { get; set; }
        public PrevQuery PreviousQuery { get; set; }

        public string Query { get; set; }

        public string JobName { get; set; }

        public bool CurrentlyRunning { get; protected set; }

        public bool ShouldRun { get; set; } = true;

        public LooperConfig Config { get; set; } = new LooperConfig();

        public virtual void Start() {}
    }
}
