﻿using LibLooper.Utils;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace LibLooper.LooperJobs
{
    public class TfsReader : Reader<TfsReader>, IReader
    {
        public delegate void QuitEventHandler();

        public TfsReader()
        {
        }

        public WorkItemStore workItemStore { get; private set; } = null;

        public override IEnumerable<T> RunQuery<T>(string query)
        {
            if(workItemStore != null)
            {
                IEnumerable<T> workItems = workItemStore.Query(query).Cast<T>();
                return workItems;
            }
            return null;
        }

        protected override void init()
        {
            if (workItemStore == null && !string.IsNullOrEmpty(m_baseUrl))
            {
                int counter = 0;
                while ((workItemStore = TfsUtils.connectAzureTfs(new Uri(m_baseUrl))) == null)
                {
                    System.Threading.Thread.Sleep(1000);
                    counter++;
                    if (counter == 300)
                    {
                        DialogResult result = MessageBox.Show("The tfs server" + m_baseUrl + " seems to be unreachable. The connection has timed out. do you want to quit the application?", "error accessing tfs server", MessageBoxButtons.YesNo);
                        if (result == DialogResult.Yes)
                        {
                            Application.Exit();
                        }
                    }
                }
            }
        }
    }
}
