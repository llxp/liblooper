﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace LibLooper.IcMAuthentication.RestModels
{
    [DataContract]
    public class IcMToken
    {
        public IcMToken()
        {
            ObjectCreatedAt = DateTime.Now;
        }
        [DataMember(Name = "access_token")]
        public string access_token { get; set; }
        [DataMember(Name = "token_type")]
        public string token_type { get; set; }
        [DataMember(Name = "expires_in")]
        public int expires_in { get; set; }
        public DateTime ObjectCreatedAt { get; set; }
    }
}
