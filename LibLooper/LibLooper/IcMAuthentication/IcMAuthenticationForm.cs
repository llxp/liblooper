﻿using LibLooper.IcMAuthentication;
using LibLooper.IcMAuthentication.RestModels;
using LibLooper.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace LibLooper
{
    public partial class IcMAuthenticationForm : Form
    {
        private string m_authenticationUri = "https://icm.ad.msft.net/sso/";
        private string m_tokenUri = "https://icm.ad.msft.net/sso/token";
        public delegate void GotTokenEventHandler(IcMToken token);
        public event GotTokenEventHandler GotToken;
        public IcMAuthenticationForm()
        {
            InitializeComponent();
            webBrowser.DocumentCompleted += WebBrowser_LocationChanged;
            webBrowser.Navigate(this.m_authenticationUri);
        }

        private HttpWebRequest createPostRequest(string uri, string cookieUri)
        {
            HttpWebRequest request = (System.Net.HttpWebRequest)(System.Net.WebRequest.Create(uri));
            request.Method = "POST";
            request.CookieContainer = IEUtils.GetUriCookieContainer(new Uri(cookieUri));
            Stream requestStream = request.GetRequestStream();
            StreamWriter writer = new StreamWriter(requestStream);
            writer.Write("grant_type=cookie");
            writer.Close();
            return request;
        }

        private void WebBrowser_LocationChanged(object sender, EventArgs e)
        {
            if(webBrowser.Url.ToString() == this.m_authenticationUri)
            {
                HttpWebRequest request = createPostRequest(this.m_tokenUri, this.m_authenticationUri);
                try
                {
                    string responseString = HttpUtils.GetResponseFromRequest(request);
                    IcMToken token = HttpUtils.DeSerializeResponse<IcMToken>(responseString);
                    GotToken?.Invoke(token);
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Exception: " + ex.Message);
                    LibLooper.Utils.Logger.logExceptionToFile("IcMAuthenticationForm.WebBrowser_LocationChanged", ex);
                }
            }
        }
    }
}
