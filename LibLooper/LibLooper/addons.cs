﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibLooper
{
    /*
     * In This class are the addons specified, which can be used inside of a query, to make the query a bit dynamical
     */
    sealed class addons
    {
        public static string today(string i)
        {
            return DateTime.Now.AddDays(Convert.ToInt32(i)).ToShortDateString();
        }
    }
}