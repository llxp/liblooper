﻿using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibLooper
{
    public class LooperMain
    {
        public LooperBacklogManager LooperManager { get; private set; }
        private SyncConfig m_config = null;
        private QueryParser m_parser = null;
        private string m_configPath = "";
        public string ConfigPath
        {
            get
            {
                return m_configPath;
            }
            set
            {
                if (m_configPath != value)
                {
                    m_config = SyncConfig.LoadConfig(value);
                }
                m_configPath = value;
            }
        }

        public LooperMain()
        {
            m_parser = new QueryParser();
            LooperManager = new LooperBacklogManager();
        }

        public void loadLooperConfig()
        {
            foreach (var looper in m_config.AllLoopers)
            {
                if (looper != null && looper.Name.Length > 0)
                {
                    if ((looper.ConfigType != null &&
                            (looper.ConfigType == "looper" || looper.ConfigType.Length == 0))
                            || looper.ConfigType == null)
                    {
                        looper.Query = m_parser.ParseQuery(looper.Query);
                        if (looper.HasPreviousQuery)
                        {
                            looper.PreviousQuery.AlternateQuery = m_parser.ParseQuery(looper.PreviousQuery.AlternateQuery);
                            looper.PreviousQuery.CheckQuery = m_parser.ParseQuery(looper.PreviousQuery.CheckQuery);
                        }
                        string name = LibLooper.Utils.Random.RandomName(10);
                        while (!LooperManager.AddQuery(looper, name))
                        {
                            name = LibLooper.Utils.Random.RandomName(10);
                        }
                        if (name.Length > 0)
                        {
                            if (looper.DefaultRunning) LooperManager.ResumeJob(name);
                        }
                    }
                }
            }
        }

        public static void loadLooperConfig(SyncConfig config, QueryParser parser, LooperBacklogManager looperManager, ContextMenuStrip menu = null, EventHandler checkedChanged = null)
        {
            foreach (var looper in config.AllLoopers)
            {
                if (looper != null && looper.Name.Length > 0)
                {
                    if ((looper.ConfigType != null && (looper.ConfigType == "looper" || looper.ConfigType.Length == 0)) || looper.ConfigType == null)
                    {
                        looper.Query = parser.ParseQuery(looper.Query);
                        if (looper.HasPreviousQuery)
                        {
                            looper.PreviousQuery.AlternateQuery = parser.ParseQuery(looper.PreviousQuery.AlternateQuery);
                            looper.PreviousQuery.CheckQuery = parser.ParseQuery(looper.PreviousQuery.CheckQuery);
                        }
                        string name = LibLooper.Utils.Random.RandomName(10);
                        while(!looperManager.AddQuery(looper, name))
                        {
                            name = LibLooper.Utils.Random.RandomName(10);
                        }
                        if (name.Length > 0)
                        {
                            if (menu != null && checkedChanged != null)
                            {
                                System.Windows.Forms.ToolStripMenuItem LooperCheckbox = new System.Windows.Forms.ToolStripMenuItem();
                                LooperCheckbox.CheckOnClick = true;
                                LooperCheckbox.Text = looper.Name;
                                LooperCheckbox.Name = name;
                                LooperCheckbox.Checked = looper.DefaultRunning;
                                LooperCheckbox.CheckedChanged += checkedChanged;
                                menu.Items.Add(LooperCheckbox);
                            }
                            if (looper.DefaultRunning) looperManager.ResumeJob(name);
                        }
                    }
                }
            }
        }
    }
}
